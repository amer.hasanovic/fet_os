FROM registry.gitlab.com/amer.hasanovic/fet_ar:latest


RUN apt-get update && apt-get upgrade -y  && \ 
     DEBIAN_FRONTEND=noninteractive apt-get -y --allow-downgrades install gdb=8.1-0ubuntu3 &&\
     apt-get clean && apt-mark hold gdb


